const Node = require('./node');
var Stack = require('stackjs');
var Queue = require('queue');
class Graph {

    constructor(edgeDirection = Graph.DIRECTED) {
        this.nodes = new Map();
        this.edgeDirection = edgeDirection;
    }

    addVertex(value) {
        if (this.nodes.has(value)) {
            return this.nodes.get(value);
        }
        const vertex = new Node(value); 
        this.nodes.set(value, vertex);
        return vertex;
    }

    removeVertex(value) {
        const current = this.nodes.get(value);
        if (current) {
            for (const node of this.nodes.values()) {
                node.removeAdjacent(current);
            }
        }
        return this.nodes.delete(value);
    }

    addEdge(source, destination) {
        const sourceNode = this.addVertex(source); 
        const destinationNode = this.addVertex(destination);
        sourceNode.addAdjacent(destinationNode);

        if (this.edgeDirection === Graph.UNDIRECTED) {
            destinationNode.addAdjacent(sourceNode); 
        }
        return [sourceNode, destinationNode];
    }


    *dfs(first) {
        const visited = new Map();
        const visitList = new Stack();
      
        visitList.push(first);
      
        while(!visitList.isEmpty()) {
            const node = visitList.pop();
            if(node && !visited.has(node)) {
                yield node;
                visited.set(node);
                node.getAdjacents().forEach(adj => visitList.push(adj));
            }
        }  
    }

    *bfsSearch(first, destination) {
        console.log("inside bfs");
        const visited = new Map();
        const visitList = new Queue();
        visitList.push(first);

        while(visitList.jobs.length != 0 && visitList.jobs[0].value != destination) {
            const node = visitList.shift();
            if(node && !visited.has(node)) {
                yield node;
                visited.set(node);
                node.getAdjacents().forEach(adj => visitList.push(adj));
            }
        }
        const node = visitList.shift(); 
        yield node;
    }

    findPath(source, destination, path = new Map()) {
        const sourceNode = this.nodes.get(source);
        const destinationNode = this.nodes.get(destination);
        const newPath = new Map(path);

        if (!destinationNode || !sourceNode) {
            return [];
        }

        newPath.set(sourceNode);
        if (source === destination) {
            return Array.from(newPath.keys());
        }

        for (const node of sourceNode.getAdjacents()) {
            if (!newPath.has(node)) {
                const nextPath = this.findPath(node.value, destination, newPath);
                if (nextPath.length) {
                    return nextPath;
                }
            }         
        }
        return [];
    }
}

Graph.UNDIRECTED = Symbol('undirected graph'); // two-way edges
Graph.DIRECTED = Symbol('directed graph'); // one-way edges

module.exports = Graph;
