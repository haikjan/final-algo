class Node {
    constructor(value) {
        this.value = value;
        this.adjacents = []
    }

    addAdjacent(node) {
        this.adjacents.push(node);
    }

    removeAdjacent(node) {
        const index = this.adjacents.indexOf(node);
        if(index > -1) {
            this.adjacents.splice(index, 1);
            return node;
        }
    }

    isAdjacent(node) {
        return this.adjacents.indexOf(node) > -1
    }

    getAdjacents(node) {
        return this.adjacents
    }
}

module.exports = Node;