const { json } = require('express');
const express = require('express')
const { groupBy } = require('lodash')
const Graph = require("./Controllers/Graph");
const app = express()
const port = 8080

app.listen(port, () => {
    console.log("Serveur à l'écoute")
    const graphbfs = new Graph(Graph.UNDIRECTED);
    const [firstBfs] = graphbfs.addEdge(1, 2);
    graphbfs.addEdge(1, 7);
    graphbfs.addEdge(1, 10);
    graphbfs.addEdge(2, 4);
    graphbfs.addEdge(4, 10);
    graphbfs.addEdge(10, 7);
    graphbfs.addEdge(4, 8);
    graphbfs.addEdge(3, 8);
    graphbfs.addEdge(8, 6);
    graphbfs.addEdge(8, 9);
    graphbfs.addEdge(9, 6);
    graphbfs.addEdge(6, 7);
    graphbfs.addEdge(5, 6);
    graphbfs.addEdge(5, 9);
    graphbfs.addEdge(5, 7);

    let bfsFromFirst = graphbfs.bfsSearch(firstBfs, 3);
    let visitedOrderBfs = Array.from(bfsFromFirst);
    const valueBfs = visitedOrderBfs.map(node => node);
    console.log("algorithme de recherche utilisant le bfs:");
    console.log(valueBfs);

    console.log("deuxieme algorithme de recherche")
    console.log(graphbfs.findPath(1, 3));
})
