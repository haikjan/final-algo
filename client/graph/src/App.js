import Graph from "react-graph-vis";

function App() {
  const graph = {
    nodes: [
      { id: 1, label: "1", title: "1" },
      { id: 2, label: "2", title: "2" },
      { id: 3, label: "3", title: "3" },                                             
      { id: 4, label: "4", title: "4" },
      { id: 5, label: "5", title: "5" },
      { id: 6, label: "6", title: "6" },
      { id: 7, label: "7", title: "7" },
      { id: 8, label: "8", title: "8" },
      { id: 9, label: "9", title: "9" },
      { id: 10, label: "10", title: "10" }
    ],
    edges: [
      { from: 1, to: 2, color: "#F2370F" },
      { from: 2, to: 1, color: "#F2370F" },
      { from: 1, to: 7, color: "#F2370F" },
      { from: 7, to: 1, color: "#F2370F" },
      { from: 1, to: 10, color: "#F2370F" },
      { from: 10, to: 1, color: "#F2370F" },
      { from: 2, to: 4, color: "#F2370F" },
      { from: 4, to: 2, color: "#F2370F" },
      { from: 4, to: 10, color: "#F2370F" },
      { from: 10, to: 4, color: "#F2370F" },
      { from: 10, to: 7, color: "#F2370F" },
      { from: 7, to: 10, color: "#F2370F" },
      { from: 4, to: 8, color: "#F2370F" },
      { from: 8, to: 4, color: "#F2370F" },
      { from: 8, to: 3, color: "#F2370F" },
      { from: 3, to: 8, color: "#F2370F" },
      { from: 8, to: 6, color: "#F2370F" },
      { from: 6, to: 8, color: "#F2370F" },
      { from: 8, to: 9, color: "#F2370F" },
      { from: 9, to: 8, color: "#F2370F" },
      { from: 9, to: 6, color: "#F2370F" },
      { from: 6, to: 9, color: "#F2370F" },
      { from: 6, to: 7, color: "#F2370F" },
      { from: 7, to: 6, color: "#F2370F" },
      { from: 6, to: 5, color: "#F2370F" },
      { from: 5, to: 6, color: "#F2370F" },
      { from: 5, to: 9, color: "#F2370F" },
      { from: 9, to: 5, color: "#F2370F" },
      { from: 5, to: 7, color: "#F2370F" },
      { from: 7, to: 5, color: "#F2370F" }
    ]
  };


  const options = {
    layout: {
      hierarchical: false
    },
    height: "500px"
  };

  const events = {
    select: function(event) {
      var { nodes, edges } = event;
    }
  };
  return (
    <Graph
      graph={graph}
      options={options}
      events={events}
    />
  );
}
export default App;